module.exports = {
  preset: 'ts-jest',
  verbose: true,
  testEnvironment: 'jsdom',
  roots: [
    '../',
  ],
  extraGlobals: ['Math', 'localStorage'],
  globalSetup: './setup.jest',
  globalTeardown: './teardown.jest',
};
