const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const rootDir = process.cwd();
const path = require('path');

const {
  NODE_ENV,
} = process.env;

const IS_DEVELOPMENT = NODE_ENV === 'development';

module.exports = [
  {
    test: /\.css$/,
    use: [
      {
        loader: MiniCssExtractPlugin.loader,
        options: {
          hmr: IS_DEVELOPMENT,
          reloadAll: true,
        },
      },
      'css-loader',
    ],
  },
  {
    test: /\.less$/,
    use: [
      {
        loader: MiniCssExtractPlugin.loader,
        options: {
          hmr: IS_DEVELOPMENT,
          reloadAll: true,
        },
      },
      'css-loader',
      'less-loader',
    ],
  },
  {
    test: /\.s[ac]ss$/,
    use: [
      {
        loader: MiniCssExtractPlugin.loader,
        options: {
          hmr: IS_DEVELOPMENT,
          reloadAll: true,
        },
      },
      'css-loader',
      'sass-loader',
    ],
  },
  {
    test: /.(png|jpg|svg|gif|ico)$/,
    use: ['file-loader'],
  },
  {
    test: /.(ttf|woff|woff2|eot)$/,
    use: ['file-loader'],
  },
  {
    test: /\.(ts|tsx)$/,
    include: [path.join(rootDir, 'config'), path.join(rootDir, 'src')],
    use: [
      {
        loader: 'ts-loader',
        options: {},
      },
    ],
  },
  {
    enforce: 'pre',
    test: /\.js$/,
    loader: 'source-map-loader',
  },
];
