const path = require('path');
const webpack = require('webpack');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const {
  NODE_ENV,
  MOCK,
  PROFILE,
  API_HOST,
  PAGES,
} = process.env;
const rootDir = process.cwd();

const IS_PRODUCTION = NODE_ENV === 'production' || PROFILE === 'PROD';

const IS_MOCK = MOCK === 'mock';

const IS_DEVELOPMENT = NODE_ENV === 'development';

const define = {
  __DEV__: JSON.stringify(IS_DEVELOPMENT),
  __MOCK__: JSON.stringify(IS_MOCK),
  __PROD__: JSON.stringify(IS_PRODUCTION),
  __PAGES__: JSON.stringify(PAGES || ''),
  __THIRD_PARTY_API__PREFIX__: PROFILE === 'PROD' ? JSON.stringify('') : JSON.stringify('/dev'),
  API_HOST,
};

console.log('define', define);

module.exports = [
  new HTMLWebpackPlugin({
    template: './layout.html',
    minify: {
      collapseWhitespace: IS_PRODUCTION,
    },
  }),
  new webpack.DefinePlugin(define),
  new CleanWebpackPlugin(),
  new CopyWebpackPlugin([
    {
      from: path.join(rootDir, './src/assets/static'),
      to: './static',
    },
  ]),
  new MiniCssExtractPlugin({
    filename: '[name].[hash].rvssvl.css',
  }),
];
