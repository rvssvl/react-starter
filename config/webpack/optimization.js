const {
  NODE_ENV,
  API_HOST,
} = process.env;

const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const IS_PRODUCTION = NODE_ENV === 'production';

module.exports = () => {
  const optimizationConfig = {
    splitChunks: {
      chunks: 'all',
    },
  };
  if (IS_PRODUCTION) {
    optimizationConfig.minimizer = [
      new OptimizeCssAssetsPlugin(),
      new TerserPlugin({
        parallel: true,
      }),
    ];
  }
  return optimizationConfig;
};
