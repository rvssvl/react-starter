const path = require('path');

const rootDir = process.cwd();
const rules = require('./rules');
const optimization = require('./optimization');
const plugins = require('./plugins');

const {
  NODE_ENV,
  MOCK,
  API_HOST,
  REMOTE_BACKEND,
  PAGES,
  PROFILE,
} = process.env;
console.log('PROFILE', PROFILE);
const IS_DEVELOPMENT = NODE_ENV === 'development';
const IS_REMOTE_BACKEND = REMOTE_BACKEND === 'remote';

module.exports = {
  context: path.join(rootDir, 'src'),
  mode: 'development',
  entry: {
    app: './index.tsx',
  },
  devtool: 'source-map',
  output: {
    filename: '[name].[hash].rvssvl.js',
    chunkFilename: '[name].[hash:8].rvssvl.chunk.js',
    path: path.join(rootDir, 'build'),
    publicPath: `${PAGES || ''}/`,
  },
  resolve: {
    extensions: ['.js', '.json', '.png', '.svg', '.css', '.less', '.ts', '.tsx'],
    alias: {
      'src': path.join(rootDir, 'src'),
    },
  },
  optimization: optimization(),
  devServer: {
    port: 7777,
    hot: IS_DEVELOPMENT,
    historyApiFallback: true,
    proxy: {
      '/api': {
        target: IS_REMOTE_BACKEND ? 'https://dev.moipavlodar.kz' : 'http://localhost:8080',
        // pathRewrite: { '^/api': '' },
        secure: false,
        logLevel: 'debug',
        changeOrigin: true,
      },
      '/dev/ac': {
        target: IS_REMOTE_BACKEND ? 'https://dev.moipavlodar.kz' : 'http://localhost:8080',
        // pathRewrite: { '^/api': '' },
        secure: false,
        logLevel: 'debug',
        changeOrigin: true,
        headers: {
          'Referer': 'https://frontend.suraqtar.dev.btsdapps.net/',
        },
      },
      '/dev/nb': {
        target: IS_REMOTE_BACKEND ? 'https://dev.moipavlodar.kz' : 'http://localhost:8080',
        // pathRewrite: { '^/api': '' },
        secure: false,
        logLevel: 'debug',
        changeOrigin: true,
        headers: {
          'Referer': 'https://dev.budget-uchastiya.kz/',
        },
      },

      '/ac': {
        target: IS_REMOTE_BACKEND ? 'https://dev.moipavlodar.kz' : 'http://localhost:8080',
        // pathRewrite: { '^/api': '' },
        secure: false,
        logLevel: 'debug',
        changeOrigin: true,
        headers: {
          'Referer': 'https://aitu.city/',
        },
      },
      '/nb': {
        target: IS_REMOTE_BACKEND ? 'https://dev.moipavlodar.kz' : 'http://localhost:8080',
        // pathRewrite: { '^/api': '' },
        secure: false,
        logLevel: 'debug',
        changeOrigin: true,
        headers: {
          'Referer': 'https://budget-uchastiya.kz/',
        },
      },
    },
  },
  plugins,
  module: {
    rules,
  },
  watchOptions: {
    ignored: /node_modules/,
  },
};
