export type uuid = string;

export interface ISerializable {
  id: uuid;
}
