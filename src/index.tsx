import * as React from 'react';
import * as ReactDOM from 'react-dom';

import axios from './services/axios';
import App from './app/index';

const initApp = () => {
  ReactDOM.render(<App />, document.getElementById('react-app'));
};

(async () => {
  if (__MOCK__) {
    await import(/* webpackChunkName: "mock" */'./services/mock')
      .then(({ configureMocks }) => configureMocks(axios));
  }
  initApp();
})();
