import apiTest from './api-test';

export interface IAPI<T> {
  actions: T;
  methods: T;
}

const API: any = {
  apiTest,
};

export default API;
