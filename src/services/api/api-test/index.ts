import { AxiosResponse } from 'axios';

import { IAPI } from '../index';
import axiosInstanceMock from '../../axios';
import { axiosInstance } from '../../axios/axios';

const axios = __MOCK__ ? axiosInstanceMock : axiosInstance;

type ApiGreet = {
  greet: ((params: { test: string }) => Promise<AxiosResponse<{ answer: string; }>>) | string;
};

const methods: ApiGreet = {
  greet: '/api/greet',
};

const actions: ApiGreet = {
  greet: (params) => {
    return axios.post(methods.greet as string, params);
  },
};

const test: IAPI<ApiGreet> = {
  actions,
  methods,
};

export default test;
