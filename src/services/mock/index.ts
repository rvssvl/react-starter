import MockAdapter from 'axios-mock-adapter';
import { AxiosInstance } from 'axios';

import apiTest from './api-test';

export function configureMocks(axiosInstance: AxiosInstance) {
  const mock = new MockAdapter(axiosInstance, { delayResponse: 500 });

  // Mock api for dealing with businesses
  console.warn('Mocks are ready...');
  apiTest(mock);
}
