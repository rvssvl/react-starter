import MockAdapter from 'axios-mock-adapter';

import API from '../api';

const headers = {
  'application/type': 'json',
};

export default function(mock: MockAdapter) {
  console.log('AppealAuth mock prepaired...');
  mock.onPost(API.test.methods.greet).reply(
    200,
    {
      answer: 'Hello!',
    },
    headers);
}
