import axios from 'axios';
import colors from 'colors';

import appSettings from '../../appSettings';

export const axiosInstance = axios.create({
  baseURL: appSettings.BASE_URL,
});

const axiosInstanceMock = axios.create({
  baseURL: appSettings.BASE_URL,
});

axiosInstanceMock.interceptors.request.use(request => {
  console.groupCollapsed();
  console.log(`%cStarting Request \n${
    JSON.stringify({
      url: request.url, params: request.params, method: request.method,
    }, null, 2)
  }`, 'background: yellow; color: #333333');
  console.groupEnd();
  return request;
});

axiosInstanceMock.interceptors.response.use(response => {
  console.groupCollapsed();
  console.log(`%cResponse: \n${
    JSON.stringify({
      status: response.status,
      data: response.data,
    }, null, 2)
  }`, 'background: yellow; color: #333333');
  console.groupEnd();
  return response;
});
if (__DEV__) {
  axiosInstance.interceptors.request.use(request => {
    console.groupCollapsed();
    console.log(`%cStarting Request \n${
      JSON.stringify({
        url: request.url, params: request.params, method: request.method,
      }, null, 2)
    }`, 'background: yellow; color: #333333');
    console.groupEnd();
    return request;
  });

  axiosInstance.interceptors.response.use(response => {
    console.groupCollapsed();
    console.log(`%cResponse: \n${
      JSON.stringify({
        status: response.status,
        data: response.data,
      }, null, 2)
    }`, 'background: yellow; color: #333333');
    console.groupEnd();
    return response;
  });
}

export default axiosInstanceMock;
