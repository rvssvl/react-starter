import { useLocation, useHistory, useParams } from 'react-router-dom';

import { GlobalStore } from '../store/global-store/global-store';

interface IAuthState {
  redirectUri: string;
  module: keyof GlobalStore;
}

interface IParams {
  module: keyof GlobalStore;
}

export function useAuthLocation(): IAuthState {
  const history = useHistory();

  const location = useLocation();

  const { module } = useParams<IParams>();

  const search = new URLSearchParams(location.search);

  if (!module) {
    history.replace('/main');
  }

  const redirectUri = search.get('redirectUri') as string;

  return {
    redirectUri, module,
  };
}
