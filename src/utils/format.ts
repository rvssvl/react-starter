import { format as formatDateFns, formatDistanceToNow } from 'date-fns';
import ruLocale from 'date-fns/locale/ru';

type FormatPrice = (price: number) => string;

export const formatPrice: FormatPrice = price => new Intl.NumberFormat('ru-RU').format(price);

type FormatDate = (date: Date) => string;

export const timeFromNow: FormatDate = (date) => {
  try {
    return formatDistanceToNow(new Date(date.getFullYear() < 2020 ? Date.parse('2020-09-26T12:58:50+06:00') : date), { locale: ruLocale, addSuffix: true });
  } catch (e) {
    return '2018';
  }
};

export const formatDateTime: (date: Date) => string = (date) => {
  return formatDateFns(new Date(date), 'dd.MM.yyyy', { locale: ruLocale });
};

export const timeLeftNumerical = (durationInSeconds: number): string => {
  // Hours, minutes and seconds
  const hrs = ~~(durationInSeconds / 3600);
  const mins = ~~((durationInSeconds % 3600) / 60);
  const secs = ~~durationInSeconds % 60;

  // Output like "1:01" or "4:03:59" or "123:03:59"
  let ret = '';

  if (hrs > 0) {
    ret += `${hrs}: ${mins < 10 ? 0 : ''}`;
  }
  if (mins > 0) {
    ret += `${mins} минут `;
  }
  ret += `${secs} секунд`;
  return ret;
};
