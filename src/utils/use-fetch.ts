/* eslint-disable no-case-declarations */
import { useEffect, useRef, useReducer } from 'react';
import { AxiosResponse } from 'axios';

interface IFetchHookState<T, R> {
  error: any;
  data: T | null;
  status: 'idle' | 'fetching' | 'error' | 'fetched';
  isFetching: boolean;
  fetch: (params: R, savePrevData?: boolean) => any;
}

type Action = { type: 'FETCHING'}
| { type: 'FETCHED'; payload: any; savePrevData?: boolean; }
| { type: 'FETCH_ERROR'; payload: any};

export const useFetch = <RETURN_TYPE, PARAMS_TYPE>(asyncFn: (params: PARAMS_TYPE) => Promise<AxiosResponse<Array<RETURN_TYPE>>>, params: PARAMS_TYPE, cached?: boolean | undefined) => {
  const cache = useRef({}) as { current: any};
  const url = '';

  const initialState: IFetchHookState<Array<RETURN_TYPE>, PARAMS_TYPE> = {
    status: 'idle',
    isFetching: false,
    error: null,
    data: null,
    fetch: (params: any, loadMore) => {},
  };

  const [state, dispatch] = useReducer((prevState: IFetchHookState<Array<RETURN_TYPE>, PARAMS_TYPE>, action: Action): IFetchHookState<Array<RETURN_TYPE>, PARAMS_TYPE> => {
    switch (action.type) {
      case 'FETCHING':
        return { ...prevState, status: 'fetching', isFetching: true };
      case 'FETCHED':
        const d = action.savePrevData ? prevState.data || [] : [];
        const newData = [...d, ...action.payload];
        return { ...prevState, status: 'fetched', data: newData, isFetching: false };
      case 'FETCH_ERROR':
        return { ...prevState, status: 'error', error: action.payload, isFetching: false };
      default:
        return state;
    }
  }, initialState);

  useEffect(() => {
    let cancelRequest = false;
    const fetchData = async (params: any, savePrevData?: boolean) => {
      dispatch({ type: 'FETCHING' });
      if (cached && cache.current[url]) {
        const data = cache.current[url];
        dispatch({ type: 'FETCHED', payload: data });
      } else {
        try {
          const response = await asyncFn(params);
          cache.current[url] = response.data;
          if (cancelRequest) return;
          dispatch({ type: 'FETCHED', payload: response.data, savePrevData });
        } catch (error) {
          if (cancelRequest) return;
          dispatch({ type: 'FETCH_ERROR', payload: error.message });
        }
      }
    };
    initialState.fetch = fetchData;
    fetchData(params);

    return function cleanup() {
      cancelRequest = true;
    };
  }, []);

  return state;
};
