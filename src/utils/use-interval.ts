import { useEffect, useRef } from 'react';

/* istanbul ignore next */
/** keep typescript happy */
const noop = () => {};

export function useInterval(
  callback: () => void,
  intervalInMillis: number | null | false,
  delay?: number,
  immediate?: boolean
) {
  const savedCallback = useRef(noop);

  // Remember the latest callback.
  useEffect(() => {
    savedCallback.current = callback;
  });

  // Execute callback if immediate is set.
  useEffect(() => {
    if (!immediate) return;
    if (intervalInMillis === null || intervalInMillis === false) return;
    savedCallback.current();
  }, [immediate]);

  // Set up the interval.
  useEffect(() => {
    if (intervalInMillis === null || intervalInMillis === false) return undefined;
    const tick = () => savedCallback.current();
    const id = setInterval(tick, intervalInMillis);
    return () => clearInterval(id);
  }, [intervalInMillis]);
}

export default useInterval;
