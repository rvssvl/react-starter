import { useEffect, useState } from 'react';
import { AxiosResponse, AxiosError } from 'axios';

interface IState<RETURN_TYPE> {
  isLoading: boolean;
  error: any;
  result: RETURN_TYPE | null;
  callAsyncFunction?: Function;
  responseHeaders?: any;
  statusCode?: number | string;
}
const INITIAL_STATE = {
  isLoading: false,
  error: null,
  result: null,
  responseHeaders: null,
};
export default function useCallApi<RETURN_TYPE, PARAMS_TYPE>(asyncFunctionToCall: (params: PARAMS_TYPE) => Promise<AxiosResponse<RETURN_TYPE>>) {
  const [state, setState] = useState<IState<RETURN_TYPE>>(INITIAL_STATE);
  useEffect(() => {
    const callAsyncFunction = async (params: PARAMS_TYPE) => {
      setState((prevState) => (
        { ...prevState, isLoading: true, result: null, error: null }
      ));
      try {
        const response = await asyncFunctionToCall(params);
        setState((prevState) => (
          { ...prevState, result: response.data, responseHeaders: response.headers, status: response.status }
        ));
      } catch (error) {
        console.log('error', error);
        setState((prevState) => (
          { ...prevState, error: error }
        ));
      } finally {
        setState((prevState) => (
          { ...prevState, isLoading: false }
        ));
      }
    };
    setState((prevState) => (
      { ...prevState, callAsyncFunction }
    ));
  }, []);
  return state;
}
