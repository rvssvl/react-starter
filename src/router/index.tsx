import React, { useEffect } from 'react';
import './animations.css';
import { BrowserRouter, Route, Switch, useLocation, Redirect, useHistory } from 'react-router-dom';
import {
  CSSTransition,
  TransitionGroup,
} from 'react-transition-group';

import MainPage from '../app/pages/main';
import { GlobalStore } from '../store/global-store/global-store';
import PrivateRoute from '../app/shared/private-route/PrivateRoute';

import useStyles from './styles';

interface IRoute {
  name: string;
  path: string;
  to?: string;
  Component: any;
  hasNestedRoutes?: boolean;
  isPrivate?: boolean;
  module?: keyof GlobalStore;
}

const Router: React.FC = () => {
  const classes = useStyles();
  return (
    <BrowserRouter>
      <AnimatedRoutes />
    </BrowserRouter>

  );
};

const prefix = __PAGES__;

export const ROUTES: Array<IRoute> = [
  { name: '-redirectMain', path: `${prefix}/*`, to: `${prefix}/main`, Component: MainPage },
  { name: 'MainPage', path: `${prefix}/main`, Component: MainPage },
];
const AnimatedRoutes = () => {
  const location = useLocation();
  const classes = useStyles();
  return (
    <TransitionGroup>
      <CSSTransition
        key={location.key}
        timeout={410}
        classNames="page"
        unmountOnExit>
        <Switch location={location}>
          {
            ROUTES.filter(r => !r.name.startsWith('-re')).map(({ path, Component, hasNestedRoutes, to, name, isPrivate, module }, index) => (
              isPrivate ? (
                <PrivateRoute key={path} exact={!hasNestedRoutes} path={path} authPath={`/auth/${module || 'appeal'}/request`} component={
                  () => (
                    <div className={`page ${classes.contentInside}`}>
                      <Component />
                    </div>
                  )
                } />
              ) : (
                <Route key={path} exact={!hasNestedRoutes} path={path}>
                  <div className={`page ${classes.contentInside}`}>
                    <Component />
                  </div>
                </Route>
              )
            ))
          }
          {
            ROUTES.filter(r => r.name.startsWith('-re')).map(({ path, to }) => (
              <Redirect from={path} to={to!} key={path} exact />
            ))
          }
        </Switch>
      </CSSTransition>
    </TransitionGroup>

  );
};

export default Router;
