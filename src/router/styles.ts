import { makeStyles } from '@material-ui/styles';
import { Theme } from '@material-ui/core';

import { HEADER_HEIGHT } from '../app/theme/layout';

const useStyles = makeStyles((theme: Theme) => ({
  router: {
    position: 'relative',
    height: document.documentElement.clientHeight,
    width: '100%',
    overflow: 'hidden',
  },
  content: {
    position: 'absolute',
    width: '100%',
    height: `calc(${document.documentElement.clientHeight}px - ${HEADER_HEIGHT}px)`,
    overflowY: 'hidden',
  },
  contentInside: {
    height: `calc(${document.documentElement.clientHeight}px - ${HEADER_HEIGHT}px)`,
    width: `100%`,
    overflowY: `auto`,
  },
  navigation: {
    position: 'absolute',
    top: `calc(100% - ${HEADER_HEIGHT}px)`,
  },
}));

export default useStyles;
