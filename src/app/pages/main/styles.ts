import { makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => (
  {
    mainContainer: {
      background: theme.palette.background.default,
    },
  }
));

export default useStyles;
