import * as React from 'react';
import { Container } from '@material-ui/core';
import { useHistory } from 'react-router-dom';

import useStyles from './styles';

const prefix = `${__PAGES__}`;

const MainPage = () => {
  const classes = useStyles();
  const history = useHistory();

  return (
    <Container maxWidth="lg" className={classes.mainContainer}>
      React App!
    </Container>

  );
};

export default MainPage;
