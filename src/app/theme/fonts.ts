
const Fonts = {
  InterLight: 'Inter-Light',
  InterBold: 'Inter-Bold',
  InterRegular: 'Inter-Regular',
  InterMedium: 'Inter-Medium',
};

export default Fonts;
