import { CreateCSSProperties, CSSProperties } from '@material-ui/styles';

type TypeLayout = {
  verticalPadding: CSSProperties;
  horizontalPadding: CSSProperties;
  displayFlexCenter: CSSProperties;
  displayFlexColumn: CSSProperties;
};

export const LAYOUT: TypeLayout = {
  verticalPadding: {
    paddingTop: `24px`,
    paddingBottom: `24px`,
  },
  horizontalPadding: {
    paddingLeft: `32px`,
    paddingRight: `32px`,
  },
  displayFlexCenter: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  displayFlexColumn: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
  },
};

export const HEADER_HEIGHT = 66;
