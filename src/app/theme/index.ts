import { createMuiTheme } from '@material-ui/core/styles';

import Fonts from './fonts';

const interRegular = {
  fontFamily: Fonts.InterRegular,
  src: `
    url('${__PAGES__}/static/fonts/Inter-SemiBold.ttf') format('truetype')
  `,
};

const interBold = {
  fontFamily: Fonts.InterBold,
  src: `
    url('${__PAGES__}/static/fonts/Inter-Bold.ttf') format('truetype')
  `,
};

const interLight = {
  fontFamily: Fonts.InterLight,
  src: `
    url('${__PAGES__}/static/fonts/Inter-Regular.ttf') format('truetype')
  `,
};

const interMedium = {
  fontFamily: Fonts.InterMedium,
  src: `
    url('${__PAGES__}/static/fonts/Inter-Medium.ttf') format('truetype')
  `,
};

const theme = createMuiTheme({
  palette: {
    background: {
      default: '#F4F5F5',
      paper: '#FFFFFF',
    },
    primary: {
      main: '#319CFF',
      dark: '#51708C',
      light: '#51D4FD',
      contrastText: '#E2DCFF',
    },
    secondary: {
      main: '#4AC8AE',
      dark: '#225F52',
      light: '#FFE359',
    },
    text: {
      primary: '#242529',
      secondary: '#686A71',
      hint: '#225F52',
    },
    divider: '#F4F5FC',
    error: {
      main: '#FF9BAD',
    },
    grey: {
      A100: '#888888',
      A200: '#C2C4CA',
    },
  },
  typography: {
    fontFamily: [Fonts.InterRegular, Fonts.InterMedium, Fonts.InterLight, Fonts.InterBold, 'sans-serif'].join(','),
    fontSize: 16,
    subtitle2: {
      fontSize: 13,
    },
    body1: {
      fontSize: 18,
      lineHeight: '24px',
    },
    body2: {
      fontSize: 15,
      lineHeight: '24px',
    },
    h6: {
      fontSize: '1.125rem',
      lineHeight: 1.111,
    },
    h4: {
      fontSize: 18,
      fontWeight: 600,
    },
    h3: {
      fontSize: 24,
      fontWeight: 600,
      lineHeight: '32px',
    },
    h2: {
      fontSize: 32,
      fontWeight: 600,
      lineHeight: '40px',
    },
    h1: {
      fontSize: 48,
      fontWeight: 'bold',
    },
  },
  spacing: 4,
  breakpoints: {

  },
});

/* theme Overrides */
theme.overrides = {
  ...theme.overrides,
  MuiCssBaseline: {
    '@global': {
      body: {
        margin: 0,
        backgroundColor: theme.palette.background.paper,
        overflow: 'hidden',
        height: '100%',
        width: '100%',
      },
      '@font-face': [interRegular, interMedium, interLight, interBold],
    },
  },
  MuiButton: {
    // Name of the rule
    containedSecondary: {
      // Some CSS
      background: theme.palette.secondary.main,
      color: theme.palette.background.paper,
      borderRadius: 86,
      fontSize: 14,
      lineHeight: '17px',
      fontWeight: 'bold',
      minWidth: 119,
      height: 43,
      boxShadow: '0px 4px 15px rgba(100, 65, 243, 0.24)',
      fontFamily: Fonts.InterBold,
    },
    containedPrimary: {
      // Some CSS
      background: theme.palette.primary.main,
      color: theme.palette.background.paper,
      fontSize: 14,
      lineHeight: '18px',
      fontWeight: 'bold',
      minWidth: 119,
      height: 43,
      boxShadow: 'none',
      fontFamily: Fonts.InterBold,
      '&:disabled': {
        color: '#ffffff',
        boxShadow: 'none',
      },
    },
    outlinedPrimary: {
      // Some CSS
      background: theme.palette.background.paper,
      color: theme.palette.primary.main,
      border: `1px solid ${theme.palette.primary.main}`,
      borderRadius: 4,
      fontSize: 14,
      lineHeight: '18px',
      fontWeight: 'bold',
      minWidth: 119,
      height: 43,
      boxShadow: 'none',
      fontFamily: Fonts.InterRegular,
    },
  },
  MuiContainer: {
    root: {
      [theme.breakpoints.down('sm')]: {
        padding: 0,
      },
    },
  },
  MuiInputLabel: {
    root: {
      fontFamily: Fonts.InterMedium,
      fontWeight: 500,
      fontSize: 14,
      lineHeight: '20px',
      color: '#1D1C28',
    },
    shrink: {
      transform: 'translate(0, -1.5px)',
    },
  },
  MuiFormControl: {
    root: {
      marginBottom: 12,
    },
  },
  MuiFormHelperText: {
    root: {
      fontFamily: Fonts.InterLight,
    },
  },
};

export default theme;
