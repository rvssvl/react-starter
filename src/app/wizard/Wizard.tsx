import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { observer } from 'mobx-react';

import { useStores } from '../../store/MobxStore';

function useUser() {
  const user = useStores().user;
  return user;
}

const Wizard: React.FC = () => {
  const history = useHistory();
  const user = useUser();
  useEffect(() => {
    if (!user.alreadyOpened) {
      history.replace('/on-boarding');
    }
  }, [user.alreadyOpened]);

  return (
    <div />
  );
};

export default observer(Wizard);
