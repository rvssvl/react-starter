import React from 'react';
import { RouteProps } from 'react-router';
import { Redirect, Route } from 'react-router-dom';

interface IStateProps extends RouteProps {
  authPath: string;
  hasPermission?: boolean;
}

type IProps = IStateProps;

const prefix = __PAGES__;

export const PrivateRoute: React.FC<IProps> = ({ component, path, authPath, hasPermission }) => {
  return hasPermission ? (
    <Route path={path} component={component} />
  ) : (
    <Redirect
      to={{
        pathname: `${authPath}`,
        search: `redirectUri=${path}`,
      }}
    />
  );
};

export default PrivateRoute;
