import * as React from 'react';
import { ThemeProvider } from '@material-ui/core/styles';
import { CssBaseline } from '@material-ui/core';

import Router from '../router/index';
import { MobXGlobalStoreProvider } from '../store/MobxStore';
import { createStores } from '../store/create-stores';
import { GlobalStore } from '../store/global-store/global-store';

import theme from './theme';

const App: React.FC = () => {
  const [mobX] = createStores(() => ([
    GlobalStore.createInstance(),
  ]));

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <MobXGlobalStoreProvider value={mobX}>
        <Router />
      </MobXGlobalStoreProvider>
    </ThemeProvider>
  );
};

export default App;
