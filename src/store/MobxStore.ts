import { createContext, useContext } from 'react';

import { GlobalStore } from './global-store/global-store';

export const MobXGlobalStoreContext = createContext<GlobalStore>(GlobalStore.createInstance());
export const MobXGlobalStoreProvider = MobXGlobalStoreContext.Provider;

export function useStores(): GlobalStore {
  return useContext(MobXGlobalStoreContext);
}
