import { useEffect, useRef, useMemo } from 'react';

import { IOnDestroy } from './on-destroy.interface';

export const createStores = <T extends Array<any>>(init: () => T): T => {
  let persistedStores = useRef<T | null>();

  useMemo(() => {
    persistedStores.current = init();
  }, [true]);

  useEffect(() => {
    return () => {
      if (persistedStores.current) {
        persistedStores.current.forEach((store: IOnDestroy) => {
          if (store.onDestroy && typeof store.onDestroy === 'function') store.onDestroy();
        });
      }

      persistedStores.current = null;
    };
  }, []);

  return persistedStores.current as unknown as T;
};
