import { IOnDestroy } from './on-destroy.interface';

export class Base implements IOnDestroy {
    protected cleanUp: Function | Function[] = () => {};

    public onDestroy = () => {
        if (Array.isArray(this.cleanUp)) {
            this.cleanUp.forEach(cleanUp => cleanUp());
        } else {
            this.cleanUp();
        }
    }
}
