import { observable } from 'mobx';
import { create, persist } from 'mobx-persist';

import { Base } from '../base';

import { UserStore } from './user';

export const hydrate = create({
  storage: localStorage,
  jsonify: true,
});

export class GlobalStore extends Base {
  @observable public user: UserStore;

  private static instance: GlobalStore | null = null;

  private constructor() {
    super();
    this.user = UserStore.createInstance();
    (window as unknown as { store: any }).store = this;
  }

  public static createInstance = () => {
    if (!GlobalStore.instance) {
      GlobalStore.instance = new GlobalStore();
    }
    return GlobalStore.instance;

  }
}
