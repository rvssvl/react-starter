import { action, observable, reaction } from 'mobx';

import { Base } from '../../base';

export class UserStore extends Base {
  @observable public alreadyOpened: boolean | string;
  @observable public name: string;
  @observable public phone: string;
  @observable public phoneVerified: boolean;

  private static instance: UserStore | null = null;
  private static key: string = 'user.store';
  private static alreadyOpenedKey: string = 'alreadyOpened';
  private static regionKey: string = 'regionKey';

  private constructor() {
    super();
    this.alreadyOpened = !!localStorage.getItem(UserStore.alreadyOpenedKey) || false;
    this.name = (localStorage.getItem(`${UserStore.key}.name`) as string) || '';
    this.phone = (localStorage.getItem(`${UserStore.key}.phone`) as string) || '';
    this.phoneVerified = (!!localStorage.getItem(`${UserStore.key}.phoneVerified`) as boolean);
    this.cleanUp = reaction(
      () => this.name,
      this.onChangeName
    );
  }

  public static createInstance = () => {
    if (!UserStore.instance) {
      UserStore.instance = new UserStore();
    }
    return UserStore.instance;

  }

  private onChangeName(name: string) {
    console.log(name);
  }
  @action
  public setAlreadyOpened(alreadyOpened: boolean | string) {
    this.alreadyOpened = alreadyOpened;
    if (alreadyOpened) {
      localStorage.setItem(UserStore.alreadyOpenedKey, alreadyOpened as string);
    } else {
      localStorage.removeItem(UserStore.alreadyOpenedKey);
    }
  }

  @action
  setName(name: string | null) {
    this.name = name || '';
    if (name) {
      localStorage.setItem(`${UserStore.key}.name`, name);
    } else {
      localStorage.removeItem(`${UserStore.key}.name`);
    }
  }

  @action
  setPhone(phone: string | null) {
    this.phone = phone || '';
    if (phone) {
      localStorage.setItem(`${UserStore.key}.phone`, phone);
    } else {
      localStorage.removeItem(`${UserStore.key}.phone`);
    }
  }

  @action
  setPhoneVerified(isVerified: boolean) {
    this.phoneVerified = isVerified;
    if (isVerified) {
      localStorage.setItem(`${UserStore.key}.phoneVerified`, 'true');
    } else {
      localStorage.removeItem(`${UserStore.key}.phoneVerified`);
    }
  }

  @action
  reset() {
    this.setName(null);
    this.setPhone(null);
    this.setPhoneVerified(false);
  }
}
