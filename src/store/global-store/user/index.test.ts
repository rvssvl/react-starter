import { UserStore } from './index';

describe('Testing UserStore', () => {
  test('does UserStore exist: ', () => {
    expect(UserStore.createInstance()).toBeInstanceOf(UserStore);
  });

  test('Name changes when setName() called ', () => {
    const store = UserStore.createInstance();
    store.setName('Rassul');
    expect(store.name).toEqual('Rassul');
  });

  test('Phone changes when setPhone() called ', () => {
    const store = UserStore.createInstance();
    store.setPhone('+77474150198');
    expect(store.phone).toEqual('+77474150198');
  });
});
