export interface IOnDestroy {
    onDestroy(): void;
}
