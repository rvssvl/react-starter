const path = require('path');
const isFIX = process.argv.includes('--fix');

module.exports = {
  'extends': [
    'eslint:recommended',
    'standard',
    'plugin:react/recommended',
    'plugin:jsx-a11y/recommended',
  ],
  'parser': 'typescript-eslint-parser',
  'parserOptions': {
    'ecmaVersion': 8,
    'sourceType': 'module',
    'ecmaFeatures': {
      'jsx': true,
    },
  },
  'plugins': [
    'react',
    'jsx-a11y',
    'import',
    'typescript',
  ],
  'settings': {
    'import/core-modules': [
      'redux-saga/effects',
    ],
    'import/parsers': {
      'typescript-eslint-parser': ['.ts', '.tsx'],
    },
    'import/resolver': {
      'webpack': {
        'config': {
          'resolve': {
            'extensions': ['.tsx', '.ts', '.js', '.css'],
            'alias': {
              'src': path.resolve('src'),
              'config': path.resolve('config'),
            }
          }
        }
      }
    }
  },
  'rules': {
    'no-undef': 0,
    'no-console': 0,
    'comma-dangle': [2, 'always-multiline'],
    'space-before-function-paren': [
      2,
      {
        'anonymous': 'never',
        'named': 'never',
        'asyncArrow': 'always',
      },
    ],
    'semi': [2, 'always'],
    'object-curly-spacing': [2, 'always'],
    'generator-star-spacing': 0,

    'react/prop-types': 0,
    'react/no-array-index-key': 2,
    'react/no-redundant-should-component-update': 2,
    'react/self-closing-comp': 2,
    'react/jsx-tag-spacing': 2,
    'react/jsx-equals-spacing': 2,
    'react/jsx-wrap-multilines': [
      2,
      {
        'declaration': 'parens-new-line',
        'assignment': 'parens-new-line',
        'return': 'parens-new-line',
        'arrow': 'parens-new-line',
        'condition': 'ignore',
        'logical': 'ignore',
        'prop': 'ignore',
      },
    ],
    'react/sort-comp': [2, {
      order: [
        'static-methods',
        'type-annotations',
        'instance-variables',
        'lifecycle',
        'everything-else',
        '/^handle.+$/',
        '/^render.+$/',
        'render'
      ]
    }],

    'import/order': [
      isFIX ? 0 : 2,
      {
        'groups': [
          'builtin',
          'external',
          'internal',
          'parent',
          'sibling',
          'index',
        ],
        'newlines-between': 'always',
      },
    ],
    'import/no-duplicates': 0,
    'import/default': 2,
    'import/no-self-import': 2,
    'import/no-useless-path-segments': 2,
    'import/export': 2,
    'import/first': 2,
    'import/newline-after-import': 2,
    'import/dynamic-import-chunkname': 2,
    'jsx-quotes': ['error', 'prefer-double'],
  },
  'overrides': {
    'files': ['*.ts', '*.tsx'],
    'rules': {
      'no-unused-vars': 0,

      'typescript/adjacent-overload-signatures': 0,
      'typescript/class-name-casing': 2,
      'typescript/interface-name-prefix': [2, 'always'],
      'typescript/member-delimiter-style': 2,
      'typescript/no-angle-bracket-type-assertion': 2,
      'typescript/no-empty-interface': 2,
      'typescript/no-unused-vars': 2,
      'typescript/no-use-before-define': 2,
      'typescript/no-var-requires': 2,
      'typescript/type-annotation-spacing': 2,
    },
  },
};
