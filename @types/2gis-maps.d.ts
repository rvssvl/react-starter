declare module '2gis-maps' {
  type Coordinates = Array<number>;

  type Map = any;

  interface IOptions {
    center: Coordinates;
    zoom: number;
  }

  interface IMarkerCoordinate {
    addTo: (map: Map) => IMarkerCoordinate;
    bindPopup: (popup: string) => void;
  }

  export function map(rootId: string, options: IOptions): Map;
  export function then(callback: () => void): void;
  export function marker(coordinates: Coordinates): IMarkerCoordinate;
}
