# Based on Node.js, to build and compile the frontend
FROM node:10 as build-stage

WORKDIR /app

COPY . /app

RUN npm install
RUN npm run build

# Based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx
COPY --from=build-stage /app/build/ /usr/share/nginx/html
# Copy the default nginx.conf provided by tiangolo/node-frontend
COPY --from=build-stage /app/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

EXPOSE 443

WORKDIR /usr/share/nginx/html
COPY ./env.sh .

# Add bash
#RUN apk add --no-cache bash

# Run script which initializes env vars to fs
RUN chmod +x env.sh

# Start Nginx server
CMD ["/bin/bash", "-c", "/usr/share/nginx/html/env.sh && nginx -g \"daemon off;\""]
